
APPNAME:=tarp

CFLAGS+=-Wall -Wextra -std=gnu11

cflags_debug:= -Og -g -march=x86-64
cflags_release:= -Os -D_FORTIFY_SOURCE=1


dir_src := src
ext_src := c
dir_target := bin

CC = gcc


#####################
#                   #
#####################

debug:
	$(CC) $(CFLAGS) $(cflags_debug) $(dir_src)/*.$(ext_src) -o $(dir_target)/$(APPNAME)


clean:
	@$(RM) -rv $(dir_target)/


.PHONY: clean
