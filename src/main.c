






#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include "parse.h"
#include "tarray.h"
#include "tarp.h"
#include "primitives.h"
#include "exception.h"
/* 
 * TODO LIST
1. add exception-handling
2. fix [???]
3. add string-handling to parser
4. make parse_file0x2 work correctly DONE
TODO 2018-09-25T1957: some simple evaluator with ability to have variables (I think the variables should work by having two arrays(or an array of pairs), that has <char_array symbol> + <variable>

*/







int main( int argc , char **argv ) {
	//types_print_all( stdout );
	
	
	
	printf( "\n" );
	struct environment env;
	int jumpcode;
	if( (jumpcode = setjmp( env.exception_environment) ) ) {
		printf( "exiting" );
		exit( EXIT_FAILURE );
	}
	struct environment *envptr = &env;
	
	char * filepath = 0;
	

	assert( argc > 0 );
	switch( argc ) {
		case 0:
			printf_warning( "fatal! unexpected argc=0!" );
			exit(EXIT_FAILURE);
		case 2:
			filepath = argv[1];
			break;
		default:
			printf( "usage: `%s <filepath>`" , argv[0]);
			exit(EXIT_FAILURE);
	}
	
	
	
	ensure( filepath != 0 , envptr);
	
	if( filepath != 0 ) {
		struct tarr content = parse_file_0x3( filepath );
		putchar( '\n' );
		
		struct tarr evaled = tarr_create( type_tarr , content.length  );
		evaled.length = evaled.capacity;
		for( size_t i = 0; i < content.length; ++i  ) {
			printf( "\n\t0x%zx.:\n" , i );
			tarr_print( stdout , &(content.arr_tarr[i]) );
			evaled.arr_tarr[i] = evaluate_0x2( &(content.arr_tarr[i]) );
			printf( "\n" );
			tarr_print( stdout , &(evaled.arr_tarr[i]) );
		}
	}
	
	printf( "\n\n" );
}









void random_bullshit_0( void ) 
{
	types_print_all( stdout );
	
	
	
	struct tarr * arr0 = tarr_new_tarr(  3);
	
	arr0->arr_tarr[0] = tarr_create_char_from_cstring( "entry #0" );
	arr0->arr_tarr[1] = tarr_create_char_from_cstring( "entry #1" );
	arr0->arr_tarr[2] = tarr_create_char_from_cstring( "entry #2" );
	arr0->length = 3;
	
	tarr_display( stdout , arr0);
	putchar( '\n' );
	tarr_print( stdout,  arr0 );
	putchar( '\n' );
	tarr_display( stdout , arr0->arr_tarr + 0 );
	putchar( '\n' );
	tarr_display( stdout , arr0->arr_tarr + 1 );
	putchar( '\n' );
	tarr_display( stdout , arr0->arr_tarr + 2 );
	
	
	putchar( '\n' );
	putchar( '\n' );
	struct tarr * arr_float_0 = tarr_new( type_float , 4 );
	tarr_display( stdout , arr_float_0 );
	arr_float_0->arr_float[0] = -(  40.0 )  / 32.0 ;
	arr_float_0->arr_float[1] = -(  80.0 )  / 32.0 ;
	arr_float_0->arr_float[2] = -( 160.0 ) / 32.0 ;
	arr_float_0->arr_float[3] = -( 320.0 ) / 32.0 ;
	arr_float_0->length = 4;
	putchar( '\n' );
	tarr_print( stdout , arr_float_0 );
	

	
	
	tarr_delete( &arr0 );
	tarr_delete( &arr_float_0 );
	printf( "\n----\n" );
}
























void test_tarr_copies(void) {
	
	
	struct tarr tarr_f0 = tarr_create_float_vector( 3 , 21.37 , 14.88 , 9.11 );
	
	
	printf( "\n\t original:" );
	tarr_display( stdout, &tarr_f0 );
	putchar('\n');
	tarr_print( stdout , &tarr_f0 );
	putchar('\n');
	
	struct tarr tarr_copies = tarr_create_tarr( 1 );
	
	tarr_tarr_stack_push( &tarr_copies , tarr_f0 ); // after dumb copy, you should "forget" the original, so as to not alias the content
	tarr_tarr_stack_emplace( &tarr_copies , &tarr_f0 ); // don't forget to set the proper pointer! because normally it only sets 'void*' pointer
	tarr_copies.arr_tarr[1].arr_float = tarr_copies.arr_tarr[1].void_ptr;
	printf( "\n\t dump copy:" );
	tarr_display( stdout, &tarr_copies.arr_tarr[0] );
	putchar('\n');
	tarr_print( stdout, &tarr_copies.arr_tarr[0] );
	printf( "\n\t shallow copy:" );
	tarr_display( stdout, &tarr_copies.arr_tarr[1] );
	putchar('\n');
	tarr_print( stdout, &tarr_copies.arr_tarr[1] );
}


void test_parsing(void)
{
	char * const fname = "txt/example-code.tarp";
	printf( "parsing '%s'.." , fname );
	struct tarr content = parse_file_0x3( fname );
	printf( " parsed content:\n" );
	tarr_print( stdout, &content );
	putchar( '\n' );
	printf( "pretty print:\n" );
	tarr_pretty_print( stdout, &content , 0 );
	printf( "\nxD\n" );
	
}


void test_random_stuff(void)
{
	struct tarr e0 = tarr_create_int32_enumerate( 5 );
	e0.arr_int32[0] = 4;
	e0.arr_int32[1] = 0;
	e0.arr_int32[2] = 1;
	e0.arr_int32[3] = 3;
	e0.arr_int32[4] = 2;
	putchar( '\n' );
	putchar( '\n' );
	tarr_print( stdout , &e0 );
	tarr_sort_in_place( &e0 );
	putchar( '\n' );
	tarr_print( stdout , &e0 );
	
	
	putchar( '\n' );
	putchar( '\n' );
	struct tarr f0 = tarr_create_float_vector( 4 , 0.75 , 0.125 , 0.5 , 0.25 );
	tarr_print( stdout , &f0 );
	tarr_sort_in_place_descending( &f0 );
	putchar( '\n' );
	tarr_print( stdout , &f0 );
}


void test_number_stuff(void) {
	struct tarr fv0 = tarr_create_float_vector( 4 , 0.25 , 0.25 , 0.5 , 0.25 );
	struct tarr f0 = tarr_sum_of_array( &fv0 );
	
	tarr_print(stdout, &fv0 );
	tarr_print(stdout, &f0 );
	
}


void tarr_sort_strings( void ) {
	struct tarr t0 = tarr_create_tarr( 5 );
	
	t0.arr_tarr[0] = tarr_create_char_from_cstring( "eeee" );
	t0.arr_tarr[1] = tarr_create_char_from_cstring( "bbbb" );
	t0.arr_tarr[2] = tarr_create_char_from_cstring( "dddd" );
	t0.arr_tarr[3] = tarr_create_char_from_cstring( "cccc" );
	t0.arr_tarr[4] = tarr_create_char_from_cstring( "aaaa" );
	t0.length = 5;
	
	tarr_display( stdout , &t0 );
	tarr_print( stdout , &(t0.arr_tarr[0]) );
	
	tarr_print( stdout , &t0 );
	
	tarr_sort_in_place( &t0 );
	tarr_print( stdout , &t0 );
	
	
	
	printf( "\n\n\n\n" );
}


void test_autoparse(void)
{
	char *s[] = {
		 "2pppp"
		,"dupa.8"
		,"0xaa"
		,"123"
		,"21.37"
		,"13.88"
	};
	struct tarr t[COUNT_OF( s )];
	
	for( size_t i = 0; i < COUNT_OF( t ); ++i ) {
		t[i] = tarr_autoparse_as_symbol( s[i] );
		printf( "\n" );
		tarr_display( stdout, &t[i] );
		printf( "\n" );
		tarr_print( stdout, &t[i] );
		printf( "\n" );
	}
}



void test_assoc(void){
	struct tarr t_strs = tarr_create_tarr( 4 );
	struct tarr t_vals = tarr_create_tarr( 4 );
	
	{
		struct tarr t_str , t_val;
		
		t_str = tarr_autoparse_as_symbol( "value-double" );
		t_val = tarr_create_double( 14.75 );
		tarr_tarr_stack_push( &t_strs , t_str );
		tarr_tarr_stack_push( &t_vals , t_val );
		
		
		t_str = tarr_autoparse_as_symbol( "some-string" );
		t_val = tarr_create_char_from_cstring( "this is the string!" );
		tarr_tarr_stack_push( &t_strs , t_str );
		tarr_tarr_stack_push( &t_vals , t_val );
		
		
		
		t_str = tarr_autoparse_as_symbol( "vector-of-int32" );
		t_val = tarr_create_int32_vector( 4 , 21 , 9 , 21 , 2 );
		tarr_tarr_stack_push( &t_strs , t_str );
		tarr_tarr_stack_push( &t_vals , t_val );
	}
	
	printf( "\n\tt_strs:" );
	tarr_print( stdout , &t_strs );
	printf( "\n\tt_vals:" );
	tarr_print( stdout , &t_vals );
	
	
	char * str = "some-string";
	printf( "\nmatching \"%s\". received:" , str );
	size_t const pos = tarr_tarr_char_find_position_of_string( t_strs , str );
	printf( " position=%#zx" , pos );
	struct tarr *ptr = tarr_tarr_get_pointer_to_position( t_vals , pos );
	printf( " pointer = %p" , ptr );
	printf( " object: \n" );
	tarr_display( stdout , ptr );
	printf( "\n" );
	tarr_print( stdout , ptr );
	
	
	printf( "\n\n\n" );
}
