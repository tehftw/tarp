#pragma once

#include "tarray.h"


typedef struct tarr (* primitive_procedure_pointer) (struct tarr * const);

struct primitive {
	int min_args;
	int max_args;
	char * name;
	primitive_procedure_pointer proc_ptr;
};









typedef struct tarr (* primitive_procedure_pointer) (struct tarr * const);

extern struct primitive TABLE_PRIMITIVE[];

struct tarr evaluate_0x2(
/* evaluates recursively */
		struct tarr * expr );

#define evaluate evaluate_0x1


