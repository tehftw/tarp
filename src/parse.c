#include "parse.h"




struct tarr read_file( FILE * in )
{
	struct tarr content = tarr_create( type_char , 0x100 );
	for( int ch = fgetc(in); ch != EOF; ch = fgetc(in) ) {
		tarr_char_stack_push(&content , ch);
	}
	tarr_char_stack_push(&content , '\0');
	return content;
}




struct tarr parse_file_0x3( char * const filename )
{
	FILE * in = fopen( filename , "r" );
	struct tarr parse_tree = tarr_create( type_tarr , 4 );
	parse_tree.arr_tarr = parse_tree.ptr;
	
	
	struct tarr current_symbol = tarr_create( type_char , 0x10 );
	
	
	// TODO (2018-09-23T16:41:03+02:00): automatically expand tarr_stack to allow infinite reading depth
	size_t tarr_stack_size = DEFAULT_READING_DEPTH;
	struct tarr ** tarr_stack = malloc( tarr_stack_size * sizeof(struct tarr*) );
	tarr_stack[0] = &parse_tree;
	
	
	
	size_t reading_depth = 0;
	bool is_reading_symbol = false;
	
	size_t line = 0;
	
	void push_current_symbol(void) {
		if( current_symbol.length < 1 ) {
			return;
		}
		if( reading_depth == 0 ) {
			printf_warning( "%s:%zd rogue symbol \"%s\". symbols should be inside a paren-delimited list!" , filename , line , current_symbol.arr_char );
		}
		tarr_char_stack_push( &current_symbol , '\0' );
		current_symbol.is_symbol = true;
		tarr_tarr_stack_emplace( tarr_stack[reading_depth] , &current_symbol );
		current_symbol = tarr_create( type_char , 0x10 );
		is_reading_symbol = false;
	}
	void increment_reading_depth(void) {
		if( reading_depth >= tarr_stack_size ) {
			tarr_stack_size *= 2;
			tarr_stack = realloc( tarr_stack , tarr_stack_size * sizeof(struct tarr *) );
			assert( tarr_stack != 0 );
		}
		
		push_current_symbol();
		struct tarr * top_of_tarr_stack = tarr_stack[reading_depth];
		
		
		struct tarr temp_tarr = tarr_create_tarr( 4 );
		assert( top_of_tarr_stack != NULL );
		tarr_tarr_stack_push(
				 top_of_tarr_stack
				,temp_tarr );
		
		
		struct tarr * new_top_of_tarr_stack =
			&(top_of_tarr_stack->arr_tarr[top_of_tarr_stack->size - 1] );
		++reading_depth;
		tarr_stack[reading_depth] = new_top_of_tarr_stack; 
	}
	void decrement_reading_depth(void) {
		if( reading_depth == 0 ) {
			printf_warning("FATAL! %s:%zd unexpected end of list: closing-paren character: ')' while already at reading_depth==0. aborting..." , filename , line );
			exit(EXIT_FAILURE);
		}
		push_current_symbol();
		--reading_depth;
	}
	
	
	
	
	
	for( int ch = fgetc(in);
	     ch != EOF;
	     ch = fgetc(in)
	     ) { switch( ch ) {
		case '(': // start of list
			increment_reading_depth();
			break;
		case ')': // end of list
			decrement_reading_depth();
			break;
		/* separators and whitespace: */
		case '\n':
			++line;
			__attribute__((fallthrough)); // this should fall through!!!
		case ' ': 
		case '\t':
		case ';':
		case ',':
			if( is_reading_symbol ) {
				is_reading_symbol = false;
				push_current_symbol();
			}
			break;
		default: // everything else gets added
			is_reading_symbol = true;
			tarr_char_stack_push( &current_symbol , ch );
	}}
	
	
	if( reading_depth > 0 ) {
		printf_warning( "FATAL end-of-file while list wasn't closed! reading_depth equals %#zx, expected 0. aborting..." , reading_depth );
		exit(EXIT_FAILURE);
	}
	
	
	fclose( in );
	free(tarr_stack);
	
	return parse_tree;
}










struct tarr parse_file_0x2( char * const filename )
{
	FILE * in = fopen( filename , "r" );
	if( in == NULL ) {
		printf_warning( "failed to open `%s`" , filename );
	}
	
	
	struct tarr parse_tree = tarr_create( type_tarr , 4 );
	parse_tree.arr_tarr = parse_tree.ptr;
	
	
	struct tarr current_symbol = tarr_create( type_char , 8 );
	
	
	// TODO (2018-09-23T16:41:03+02:00): automatically expand tarr_stack to allow infinite reading depth
	size_t tarr_stack_size = DEFAULT_READING_DEPTH;
	struct tarr ** tarr_stack = calloc( tarr_stack_size , sizeof(struct tarr*) );
	tarr_stack[0] = &parse_tree;
	
	
	
	size_t reading_depth = 0;
	bool is_reading_symbol = false;
	
	size_t line = 0;
	
	void push_current_symbol(void) {
		if( current_symbol.length < 1 ) {
			return;
		}
		if( reading_depth == 0 ) {
			printf_warning( "%s:%zd rogue symbol \"%s\". symbols should be inside a paren-delimited list!" , filename , line , current_symbol.arr_char );
		}
		tarr_char_stack_push( &current_symbol , '\0' );
		tarr_autoparse_into_symbol_inplace( &current_symbol );
		tarr_tarr_stack_emplace( tarr_stack[reading_depth] , &current_symbol );
		current_symbol = tarr_create( type_char , 8 );
		is_reading_symbol = false;
	}
	void increment_reading_depth(void) {
		if( reading_depth > tarr_stack_size ) {
			int const max_depth = DEFAULT_READING_DEPTH;
			abort_program_with_message(
					"%s:%zd reading_depth exceeds default_reading_depth(%#x)" , filename , line , max_depth );
		}
		
		push_current_symbol();
		struct tarr * top_of_tarr_stack = tarr_stack[reading_depth];
		
		struct tarr temp_tarr = tarr_create_tarr( 4 );
		tarr_tarr_stack_push(
				 top_of_tarr_stack 
				,temp_tarr );
		
		struct tarr * new_top_of_tarr_stack =
			&(top_of_tarr_stack->arr_tarr[top_of_tarr_stack->size - 1] );
		++reading_depth;
		tarr_stack[reading_depth] = new_top_of_tarr_stack; 
	}
	void decrement_reading_depth(void) {
		push_current_symbol();
		--reading_depth;
	}
	
	
	
	
	
	for( int ch = fgetc(in);
	     ch != EOF;
	     ch = fgetc(in)
	     ) { switch( ch ) {
		case '(': // start of list
			increment_reading_depth();
			break;
		case ')': // end of list
			decrement_reading_depth();
			break;
		/* separators and whitespace: */
		case '\n':
			++line;
			__attribute__((fallthrough)); // this should fall through!!!
		case ' ': 
		case '\t':
		case ';':
		case ',':
			if( is_reading_symbol ) {
				is_reading_symbol = false;
				push_current_symbol();
			}
			break;
		default: // everything else gets added
			is_reading_symbol = true;
			tarr_char_stack_push( &current_symbol , ch );
	}}
	
	
	if( reading_depth > 0 ) {
		printf_warning( "end-of-file while list wasn't closed! reading_depth equals %#zx, expected 0" , reading_depth );
	}
	
	
	fclose( in );
	free(tarr_stack);
	
	printf("\n\tfinished!");
	return parse_tree;
}






struct tarr parse_file_0x1( char * const filename )
{
	FILE * in = fopen( filename , "r" );
	struct tarr parse_tree = tarr_create( type_tarr , 4 );
	parse_tree.arr_tarr = parse_tree.ptr;
	
	
	struct tarr current_symbol = tarr_create( type_char , 8 );
	
	
	// TODO (2018-09-23T16:41:03+02:00): automatically expand tarr_stack to allow infinite reading depth
	size_t tarr_stack_size = DEFAULT_READING_DEPTH;
	struct tarr ** tarr_stack = calloc( tarr_stack_size , sizeof(struct tarr*) );
	tarr_stack[0] = &parse_tree;
	
	
	
	size_t reading_depth = 0;
	bool is_reading_symbol = false;
	
	size_t line = 0;
	
	void push_current_symbol(void) {
		if( current_symbol.length < 1 ) {
			return;
		}
		if( reading_depth == 0 ) {
			printf_warning( "%s:%zd rogue symbol \"%s\". symbols should be inside a paren-delimited list!" , filename , line , current_symbol.arr_char );
		}
		tarr_char_stack_push( &current_symbol , '\0' );
		current_symbol.is_symbol = true;
		tarr_tarr_stack_emplace( tarr_stack[reading_depth] , &current_symbol );
		current_symbol = tarr_create( type_char , 8 );
		is_reading_symbol = false;
	}
	void increment_reading_depth(void) {
		if( reading_depth > tarr_stack_size ) {
			int const max_depth = DEFAULT_READING_DEPTH;
			abort_program_with_message(
					"%s:%zd reading_depth exceeds default_reading_depth(%#x)" , filename , line , max_depth );
		}
		
		push_current_symbol();
		struct tarr * top_of_tarr_stack = tarr_stack[reading_depth];
		
		
		struct tarr temp_tarr = tarr_create_tarr( 4 );
		tarr_tarr_stack_push(
				 top_of_tarr_stack 
				,temp_tarr );
		
		
		struct tarr * new_top_of_tarr_stack =
			&(top_of_tarr_stack->arr_tarr[top_of_tarr_stack->size - 1] );
		++reading_depth;
		tarr_stack[reading_depth] = new_top_of_tarr_stack; 
	}
	void decrement_reading_depth(void) {
		push_current_symbol();
		--reading_depth;
	}
	
	
	
	
	
	for( int ch = fgetc(in);
	     ch != EOF;
	     ch = fgetc(in)
	     ) { switch( ch ) {
		case '(': // start of list
			increment_reading_depth();
			break;
		case ')': // end of list
			decrement_reading_depth();
			break;
		/* separators and whitespace: */
		case '\n':
			++line;
			__attribute__((fallthrough)); // this should fall through!!!
		case ' ': 
		case '\t':
		case ';':
		case ',':
			if( is_reading_symbol ) {
				is_reading_symbol = false;
				push_current_symbol();
			}
			break;
		default: // everything else gets added
			is_reading_symbol = true;
			tarr_char_stack_push( &current_symbol , ch );
	}}
	
	
	if( reading_depth > 0 ) {
		printf_warning( "end-of-file while list wasn't closed! reading_depth equals %#zx, expected 0" , reading_depth );
	}
	
	
	fclose( in );
	free(tarr_stack);
	
	printf("\n\tfinished!");
	return parse_tree;
}
