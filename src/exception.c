#include "exception.h"





void __teh_throw_exception_with_object(
		 struct environment * env
		,struct tarr * obj
		,char * const format 
		, ... )
{
	fputs("exception: " , stderr );
	
	if( obj != 0 ) {
		tarr_display( stderr ,  obj );
		fputc( ' ' , stderr );
	}
	
	va_list args;
	va_start(args, format);
	vfprintf(stderr, format, args);
	va_end(args);
	
	fputc( '\n' , stderr );
	longjmp( env->exception_environment , 1 );
}
