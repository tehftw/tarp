#pragma once

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>

#include "tarray.h"


#define DEFAULT_READING_DEPTH 0x2

struct tarr read_file( FILE * in );

#define parse_file parse_file_0x3
struct tarr parse_file_0x1( char * const filename );
struct tarr parse_file_0x2(
/* 0x2: auto-parses symbols into one of (double , int64 , uin64) if applicable */ 
		char * const filename );

struct tarr parse_file_0x3(
/* allows arbitrarily high reading_depth  */
		char * const filename );


/* TODO parsers:
 * 1. add different delimiters: "{}" , "[]" , "<>"
 * 2. add proper string-reading with delimited '\n' '\t' etc.
 * 3. add arbitrary, dynamically allocated depth instead of a pre-set one
 *
 * */
