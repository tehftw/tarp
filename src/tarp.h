#pragma once

#include <stdlib.h>
#include <stdio.h>




/*
		from: https://stackoverflow.com/questions/3219393/stdlib-and-colored-output-in-c 
*/
#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_RESET   "\x1b[0m"


#define DECIMAL_SEPARATOR '.'

/* from https://stackoverflow.com/questions/4415524/common-array-length-macro-for-c */
#define COUNT_OF(x) ((sizeof(x)/sizeof(0[x])) / ((size_t)(!(sizeof(x) % sizeof(0[x])))))



#define printf_warning(format , ...) \
	do { \
		fprintf(stderr , "\nwarning! %s() in file `%s`: " format "\n", __func__ ,__FILE__ ,  ##__VA_ARGS__ ); \
	} while(0)


/* TODO 2018-09-25T2356 change teh_assert macro so that it raises exception( also TODO: exceptions )  */
#define teh_assert( cond ) if( !(cond) ) { \
		fprintf(stderr , "\nassertion failure:" #cond "  %s() in file %s\n", __func__ ,__FILE__ ); \
	}

#define ensure( condition , env ) \
	if( !(condition) ) { \
		fprintf( stderr , "condition failed: " #condition ", performing longjmp" ); \
		longjmp( env->exception_environment , 1 ); \
	}


#define abort_program_with_message( format , ... ) \
	do { \
		fprintf(stderr , "\n" "FATAL!" " %s() in file `%s`: " format ". aborting program...\n", __func__  ,__FILE__ ,  ##__VA_ARGS__ ); \
		exit(EXIT_FAILURE); \
	} while(0)





#define teh_calloc_type( T , NUM ) calloc( NUM , (sizeof(T)) )

#define teh_realloc( PTR , NUM ) PTR = realloc(PTR , sizeof(typeof(PTR)))

#define teh_free( PTR ) do { \
		if( PTR == NULL ) { \
			printf_warning( "tried to free nullpointer" ); \
		} else { \
			free(PTR); \
			PTR = NULL; \
		} \
	} while(0)
