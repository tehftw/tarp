#include "tarray.h"



static size_t const TABLE_OBJTYPE_SIZE [TYPES_COUNT] = {
	 [type_null] = 0
	,[type_tarr] = sizeof(struct tarr)
	,[type_float] = sizeof(float)
	,[type_int32]  = sizeof(int32_t)
	,[type_uint32]  = sizeof(uint32_t)
	,[type_char] = sizeof(char)
	,[type_reference_tarr] = 0
	,[type_int64] = 0
	,[type_uint64] = 0
	,[type_double] = 0
};
bool is_valid_type( enum TarrType const type )
{
	return (type > type_null)
	    && (type < TYPES_COUNT);
}
bool type_is_array( enum TarrType const type ) {
	return (type > type_null)
	    && (type < TYPES_COUNT)
	    && (TABLE_OBJTYPE_SIZE[type] > 0);
}
static char const * const TABLE_OBJTYPE_NAME [TYPES_COUNT] = {
	 [type_null] = "null"
	,[type_tarr] = "tarr"
	,[type_float] = "float"
	,[type_int32]  = "int32"
	,[type_uint32]  = "uint32"
	,[type_reference_tarr] = "reference"
	,[type_char] = "char"
	,[type_int64] = "int64"
	,[type_uint64] = "uint64"
	,[type_double] = "double"
};
const char * type_get_name( enum TarrType const type )
{
	if( is_valid_type(type) ) {
		return TABLE_OBJTYPE_NAME[type];
	}
	printf_warning( "FATAL tried to get name of invalid type %#x. aborting..." , type );
	exit(EXIT_FAILURE);
}
static bool const TABLE_OBJTYPE_IS_NUMBER [TYPES_COUNT] = {
	 [type_null] = false
	,[type_tarr] = false
	,[type_float] = true
	,[type_int32]  = true
	,[type_uint32]  = true
	,[type_char] = false
	,[type_int64] = true
	,[type_uint64] = true
	,[type_double] = true
};
bool type_is_number( enum TarrType const type )
{
	if( type < type_null || type >= TYPES_COUNT ) {
		return false;
	} else {
		return TABLE_OBJTYPE_IS_NUMBER[type];
	}
}

static_assert( sizeof(TABLE_OBJTYPE_SIZE)/sizeof(TABLE_OBJTYPE_SIZE[0] ) == TYPES_COUNT , "size of array TABLE_OBJTYPE_SIZE doesn't equal TYPES_COUNT (potential source of bugs!)" );
static_assert( sizeof(TABLE_OBJTYPE_SIZE)/sizeof(TABLE_OBJTYPE_SIZE[0] ) == TYPES_COUNT , "size of array TABLE_OBJTYPE_NAME doesn't equal TYPES_COUNT (potential source of bugs!)" );



void types_print_all( FILE * out )
{
	fprintf( out , "{types: (count : %#x) size=0 means non-array\n" , TYPES_COUNT );
	for( size_t i = 0; i < TYPES_COUNT; ++i ) {
		fprintf( out , "\t<0x%zx(%s)  size:%#zx "
				, i 
				, TABLE_OBJTYPE_NAME[i]
				, TABLE_OBJTYPE_SIZE[i] );
		fprintf( out , "number?%d>\n" , TABLE_OBJTYPE_IS_NUMBER[i]);
	}
	fprintf( out , "}\n");
}


bool tarr_has_valid_type( const struct tarr * const tarr )
{
	if( tarr == NULL ) {
		return false;
	}
	return is_valid_type(tarr->type);
}

bool is_number_type( enum TarrType const type )
{
	return TABLE_OBJTYPE_IS_NUMBER[type];
}
bool tarr_has_number_type( const struct tarr * const tarr )
{
	if( tarr == NULL ) {
		return false;
	}
	return is_number_type( tarr->type );
}
bool tarr_is_array( const struct tarr * const obj )
{
	if( obj == NULL ) {
		return false;
	}
	return ( ( obj->ptr != NULL )
	      && (TABLE_OBJTYPE_SIZE[obj->type]));
}



void tarr_display( FILE * out , struct tarr * obj )
{
	if(obj == 0) {
		fprintf( out , "(null)" );
	} else {
		fputc( '(' , out );
		if( obj->is_symbol ) {
			fputs( "symbol" , out );
		}
		fprintf( out , " @%p", obj);
		fprintf( out , " type:%#x(%s) " , obj->type , TABLE_OBJTYPE_NAME[obj->type] );
		if(  tarr_is_array(obj) ) {
			fprintf( out , "ptr:%p %#zx/%#zx" , obj->ptr , obj->length , obj->capacity );
		}
		fputc( ')' , out );
	}
}




void tarr_print( FILE * out , struct tarr * obj )
{
	if( obj == 0 ) {
		fprintf( out , "<null>" );
	} else if( obj->type == type_null) { 
		fprintf( out , "<%p unitialized>" , obj );
	} else { switch(obj->type) {
		case type_float:
			fputc( '[' , out );
			for( size_t i = 0; i < obj->length; ++i ) {
				fprintf( out, " %f " , obj->arr_float[i] );
			}
			fputc( ']' , out );
			break;
		case type_int32:
			fputc( '[' , out );
			for( size_t i = 0; i < obj->length; ++i ) {
				fprintf( out, " %d " , obj->arr_int32[i] );
			}
			fputc( ']' , out );
			break;
		case type_uint32:
			fputc( '[' , out );
			for( size_t i = 0; i < obj->length; ++i ) {
				fprintf( out, " %#x " , obj->arr_uint32[i] );
			}
			fputc( ']' , out );
			break;
		case type_tarr:
			fputc( '(' , out );
			for( size_t i = 0; i < obj->length; ++i ) {
				fputc( ' ' , out );
				tarr_print( out , obj->arr_tarr + i );
				fputc( ' ' , out );
			}
			fputc( ')' , out );
			break;
		case type_char:
			if( obj->is_symbol ) {
				fprintf( out , "'%s" , obj->arr_char);
			} else {
				fprintf( out , "\"%s\"" , obj->arr_char);
			}
			break;
		case type_reference_tarr:
			fprintf( out , "<reference to %p>" , obj->reference_tarr );
			break;
		case type_int64:
			fprintf( out , "%ld" , obj->val_int64 );
			break;
		case type_uint64:
			fprintf( out , "%#lx" , obj->val_uint64 );
			break;
		case type_double:
			fprintf( out , "%f" , obj->val_double );
			break;
		default:
			tarr_display( out,  obj );
	} }
}




void indentize( FILE * out , size_t const indent ) {
	fprintf( out , "\n" );
	for( size_t i = 0; i < indent; ++i ) {
		fprintf( out , "  " );
	}
}
void tarr_pretty_print( FILE * out , struct tarr * obj , size_t const indent )
{
	if( obj->type == type_tarr ) {
		indentize( out , indent );
		if( obj->length == 0 ) {
			fprintf( out , "()" );
		} else {
			fprintf( out , "(" );
			for( size_t i = 0; i < obj->length; ++i ) {
				tarr_pretty_print( out , &(obj->arr_tarr[i]) , indent + 1 );
			}
			indentize( out , indent );
			fprintf( out , ")" );
		}
	} else {
		indentize( out , indent );
		tarr_print( out ,  obj );
	}
}




void tarr_allocate( struct tarr * tarr , size_t const size )
{
	if( tarr == 0 ) {
		printf_warning( "tried to tarr_allocate(%p , %#zx) null tarr" , tarr , size );
		return;
	}
	if( TABLE_OBJTYPE_SIZE[tarr->type] == 0 ) {
		printf_warning( "tried to tarr_allocate(%p , %#zx) type %#x which has size 0" , tarr , size , tarr->type );
		return;
	}
	if(tarr->ptr != 0) {
		printf_warning( "tried to tarr_allocate(%p , %#zx) a tarr which already has ptr: @%p" , tarr , size , tarr->ptr);
		return;
	}
	
	
	void * ptr = calloc( TABLE_OBJTYPE_SIZE[tarr->type] , size );
	assert( ptr != 0 );
	
	tarr->capacity = size;
	tarr->ptr = ptr;
	tarr_autoset_pointer( tarr );
}





void tarr_reallocate( struct tarr * tarr , size_t const size )
{
	if( tarr == 0 ) {
		printf_warning( "tried to tarr_allocate(%p , %#zx) null tarr" , tarr , size );
		return;
	}
	if( TABLE_OBJTYPE_SIZE[tarr->type] == 0 ) {
		printf_warning( "tried to tarr_allocate(%p , %#zx) type %#x which has size 0" , tarr , size , tarr->type );
		return;
	}
	
	void * const ptr = realloc( tarr->ptr , size * TABLE_OBJTYPE_SIZE[tarr->type] );
	assert( ptr != 0 );
	if( ptr != tarr->ptr ) {
		tarr->ptr = ptr;
		tarr_autoset_pointer( tarr );
	}
	tarr->capacity = size;
}
void tarr_reallocate_autogrow( struct tarr * tarr )
{
	tarr_reallocate( tarr , 2 * tarr->capacity );
}
void tarr_autogrow_if_needed( struct tarr * tarr )
{
	if( tarr->size >= tarr->capacity ) {
		tarr_reallocate_autogrow( tarr );
	}
}


void tarr_autoset_pointer( struct tarr * tarr )
{
	assert( tarr_has_valid_type(tarr) );
	assert( tarr->ptr != 0 );
	
	switch( tarr->type ) {
		case type_tarr:
			tarr->arr_tarr = tarr->ptr;
			break;
		case type_float:
			tarr->arr_float = tarr->ptr;
			break;
		case type_int32:
			tarr->arr_int32 = tarr->ptr;
			break;
		case type_uint32:
			tarr->arr_uint32 = tarr->ptr;
			break;
		case type_char:
			tarr->arr_char = tarr->ptr;
			break;
		default:
			printf_warning( "not prepared to autoset type %#x \"%s\"" , tarr->type , TABLE_OBJTYPE_NAME[tarr->type] );
	}
}




struct tarr tarr_create_empty( enum TarrType const type )
{
	assert( is_valid_type( type ) );
	
	struct tarr tarr = {
		 .type = type
		,.length = 0
		,.capacity = 0
		,.ptr = 0
		,.void_ptr = 0
		,.is_symbol = false
	};
	
	
	return tarr;
}


struct tarr tarr_create(enum TarrType const type, size_t const capacity) {
	assert( is_valid_type( type ) );
	
	if( capacity == 0 ) {
		return tarr_create_empty( type );
	}
	
	
	struct tarr tarr = tarr_create_empty( type );
	tarr_allocate(&tarr , capacity);
	
	
	return tarr;
}


struct tarr * tarr_new( enum TarrType const type , size_t const capacity )
{
	assert(type < TYPES_COUNT);
	assert(capacity > 0);
	
	struct tarr *obj = malloc( sizeof(struct tarr) );
	assert( obj != 0 );
	
	*obj = tarr_create( type , capacity );
	
	return obj;
}


void tarr_deallocate( struct tarr * obj )
{
	if( obj == 0 ) {
		printf_warning( "tried to deallocate null" );
	}
	
	if( obj->type == type_tarr ) {
		for( size_t i = 0; i < obj->length; ++i ) {
			tarr_deallocate( obj->arr_tarr + i );
		}
	}
	
	free( obj->ptr );
}

void tarr_deallocate_and_set(struct tarr * obj)
{
	assert( obj != 0 );
	tarr_deallocate( obj );
	
	obj->length = 0;
	obj->capacity = 0;
	obj->ptr = 0;
	obj->void_ptr = 0;
}

void tarr_delete( struct tarr ** obj )
{
	assert( obj != 0 );
	assert( *obj != 0 );
	tarr_deallocate( *obj );
	free( *obj );
	*obj = 0;
}



struct tarr tarr_create_tarr( size_t const capacity )
{
	struct tarr obj = tarr_create( type_tarr , capacity );
	obj.arr_tarr = obj.ptr;
	return obj;
}
struct tarr * tarr_new_tarr( size_t const capacity )
{
	struct tarr * obj = tarr_new( type_tarr , capacity );
	obj->arr_tarr = obj->ptr;
	return obj;
}




struct tarr tarr_create_reference_tarr( struct tarr * ptr_tarr )
{
	struct tarr obj = { 
		 .type = type_reference_tarr
		,.ptr = 0
		,.reference_tarr = ptr_tarr
	};
	return obj;
}
struct tarr tarr_create_int64( int_least64_t const n )
{
	struct tarr obj = {
		 .type = type_int64
		,.ptr = 0
		,.val_int64 = n
	};
	return obj;
}
struct tarr tarr_create_uint64( uint_least64_t const n )
{
	struct tarr obj = {
		 .type = type_uint64
		,.ptr = 0
		,.val_uint64 = n
	};
	return obj;
}
struct tarr tarr_create_double( double const n )
{
	struct tarr obj = {
		 .type = type_double
		,.ptr = 0
		,.val_double = n
	};
	return obj;
}


struct tarr tarr_create_float( float const f )
{
	struct tarr obj = tarr_create( type_float , 1 );
	obj.arr_float[0] = f;
	obj.length = 1;
	return obj;
}
struct tarr tarr_create_float_vector( size_t const len , ... )
{
	struct tarr fvec = tarr_create( type_float , len );
	
	va_list args;
	va_start( args , len );
	for( size_t i = 0; i < len; ++i ) {
		fvec.arr_float[i] = va_arg( args , double );
	}
	fvec.length = len;
	
	va_end( args );
	return fvec;
}



struct tarr tarr_create_int32( int32_t const n )
{
	struct tarr obj = tarr_create( type_int32 , 1 );
	obj.arr_int32[0] = n;
	obj.length = 1;
	return obj;
}
struct tarr tarr_create_int32_vector( size_t const len , ... )
{
	struct tarr ivec = tarr_create( type_int32 , len );
	
	va_list args;
	va_start( args , len );
	for( size_t i = 0; i < len; ++i ) {
		ivec.arr_int32[i] = va_arg( args , int32_t );
	}
	ivec.length = len;
	
	va_end( args );
	return ivec;
}
struct tarr tarr_create_int32_enumerate( size_t const len )
{
	struct tarr obj = tarr_create( type_int32 , len );
	
	for( size_t i = 0; i < len; ++i ) {
		obj.arr_int32[i] = i;
	}
	obj.size = len;
	
	return obj;
}




struct tarr tarr_create_uint32( uint32_t const n )
{
	struct tarr obj = tarr_create( type_uint32 , 1 );
	obj.arr_uint32[0] = n;
	obj.length = 1;
	return obj;
}
struct tarr tarr_create_uint32_vector( size_t const len , ... )
{
	struct tarr uvec = tarr_create( type_uint32 , len );
	
	va_list args;
	va_start( args , len );
	for( size_t i = 0; i < len; ++i ) {
		uvec.arr_int32[i] = va_arg( args , uint32_t );
	}
	uvec.length = len;
	
	va_end( args );
	return uvec;
}








struct tarr tarr_create_char_from_cstring( const char * const cstr )
{
	size_t const len = strlen(cstr);
	
	struct tarr str = tarr_create( type_char , len + 1 );
	
	str.arr_char = str.ptr;
	strcpy( str.arr_char , cstr );
	str.arr_char[len] = '\0';
	str.length = len;
	
	return str;
}

struct tarr tarr_create_char_wrap_cstring( char * cstr )
{
	struct tarr str = tarr_create_empty( type_char );
	str.arr_char = cstr;
	str.capacity = strlen( cstr );
	str.length = str.capacity;
	str.ptr = cstr;
	return str;
}


struct tarr tarr_dumb_copy( const struct tarr * tarr )
{
	assert( tarr != NULL );
	return *tarr;
}


struct tarr tarr_shallow_copy(  const struct tarr * tarr )
{
	assert( tarr != NULL );
	assert( is_valid_type(tarr->type) );
	
	if( !( tarr_is_array(tarr) ) ) {
		return *tarr;
	}
	
	
	struct tarr copy = tarr_create( tarr->type , tarr->capacity );
	if( tarr->ptr != 0 ) {
		memcpy( copy.ptr , tarr->ptr,  TABLE_OBJTYPE_SIZE[tarr->type] );
	}
	tarr_autoset_pointer( &copy );
	copy.is_symbol = tarr->is_symbol;
	return copy;
}


struct tarr tarr_copy( const struct tarr * tarr )
{
	assert( tarr != NULL );
	assert( tarr_has_valid_type(tarr) );
	
	if( !tarr_is_array(tarr) ) {
		return *tarr;
	}
	
	if( tarr->type != type_tarr ) {
		return tarr_shallow_copy(tarr);
	}
	
	if( tarr->type == type_tarr ) {
		struct tarr copy = tarr_create( tarr->type , tarr->capacity );
		for( size_t i = 0; i < tarr->length; ++i ) {
			copy.arr_tarr[i] = tarr_copy( &(tarr->arr_tarr[i]) );
		}
		copy.length = tarr->length;
		return copy;
	}
	
	printf_warning( "tried to copy invalid type %#x" , tarr->type );
	exit(EXIT_FAILURE);
}















void tarr_char_stack_push( struct tarr * tarr , char const ch )
{
	assert( tarr != 0 );
	assert( tarr->arr_char != 0 );
	assert( tarr->type == type_char );
	
	if( !(tarr->capacity > tarr->length) ) {
		tarr_reallocate_autogrow( tarr );
	}
	tarr->arr_char[tarr->length] = ch;
	++(tarr->length);
}


void tarr_tarr_stack_push(
		 struct tarr * tarr
		,struct tarr copied )
{
	assert( tarr != 0 );
	assert( tarr->type == type_tarr );
	
	if( !(tarr->capacity > tarr->length) ) {
		tarr_reallocate_autogrow( tarr );
	}
	
	tarr->arr_tarr[tarr->length] = copied;
	++(tarr->length);
}
void tarr_tarr_stack_emplace(
		 struct tarr * tarr
		,struct tarr * copied )
{
	assert( tarr != 0 );
	assert( copied != 0 );
	assert( tarr->type == type_tarr );
	assert( tarr_has_valid_type(copied) );
	
	tarr_autogrow_if_needed( tarr );
	
	if( copied->ptr != NULL  ) {
		size_t const member_size = TABLE_OBJTYPE_SIZE[copied->type];
		void * ptr = calloc( copied->size , member_size );
		assert( ptr != 0 );
		memcpy( ptr , copied->ptr , member_size * copied->length );
	}
	
	tarr->arr_tarr[tarr->length] = * copied;
	++(tarr->length);
}
void tarr_tarr_stack_push_dumb_copy( struct tarr * tarr , struct tarr * copied )
{
	assert( tarr != 0 );
	assert( copied != 0 );
	assert( tarr->type == type_tarr ); 
	
	
	if( !(tarr->capacity > tarr->length) ) {
		tarr_reallocate_autogrow( tarr );
	}
	
	tarr->arr_tarr[tarr->length] = * copied;
	++(tarr->length);
}

void tarr_tarr_stack_push_shallow_copy(
		 struct tarr * tarr
		,struct tarr * copied )
{
	assert( tarr != 0 );
	assert( copied != 0 );
	assert( tarr->type == type_tarr );
	assert( tarr_has_valid_type(copied) );
	
	tarr_autogrow_if_needed( tarr );
	
	if( copied->ptr != NULL  ) {
		size_t const member_size = TABLE_OBJTYPE_SIZE[copied->type];
		void * ptr = calloc( copied->size , member_size );
		assert( ptr != 0 );
		memcpy( ptr , copied->ptr , member_size * copied->length );
	}
	
	tarr->arr_tarr[tarr->length] = * copied;
	++(tarr->length);
}













/* SORTING */
bool tarr_is_sortable( struct tarr * tarr )
{
	/*
	*     sortable objects are those with size > 0 while being one of the following:
	*  char arrays;
	*  arrays of numbers;
	*  tarrays of strings;
	*/
	
	if( !(tarr->length > 0) ) {
		printf_warning( "unsortable tarr @%p, which has length=%#zx (expected: length > 0)" , tarr , tarr->length );
		return false;
	}
	if( !tarr_has_valid_type(tarr) ) {
		printf_warning( "unsortable tarr @%p, which has invalid type=%#x" , tarr , tarr->type );
		return false;
	}
	if( tarr_has_number_type( tarr ) ) {
		return true;
	}
	
	// TODO 2018-09-27T12:14:15+02:00: make tarrs of type_int64, type_uint64, and type_double sortable
	// NOTE: making them sortable would require reworking how tarrs work. namely, it would require adding homogenic tarrs. This shouldn't be hard on it's own, however it would be difficult to make it into a proper type-system. e.g. 
	
	if( tarr->type == type_tarr) {
		for( size_t i = 0; i < tarr->size; ++i ) {
			if( tarr->arr_tarr[i].type != type_char ) {
				printf_warning( "unsortable tarr @%p of type=%#x(%s), but which has a non-char_array tarr at position [%#zx] (@%p, type=%#x)"
						, tarr
						, type_tarr
						, TABLE_OBJTYPE_NAME[type_tarr]
						, i
						, &(tarr->arr_tarr[i])
						, tarr->arr_tarr[i].type );
				return false;
			}
		}
		return true;
	}
	
	
	printf_warning( "unsortable tarr: @%p has unexpected type %#x" , tarr , tarr->type  );
	return false;
}


typedef int (*comparison_function)(const void *, const void*);
int comparison_function_ascending_float (const void * p_0, const void * p_1)
{
	float const a0 = *(const float *) p_0;
	float const a1 = *(const float *) p_1;
	
	if( a0 < a1 ) {
		return -1;
	} else if( a0 > a1 ) {
		return 1;
	} else {
		return 0;
	}
}
int comparison_function_ascending_int32 (const void * p_0, const void * p_1)
{
	int32_t const a0 = *(const int32_t *) p_0;
	int32_t const a1 = *(const int32_t *) p_1;
	
	if( a0 < a1 ) {
		return -1;
	} else if( a0 > a1 ) {
		return 1;
	} else {
		return 0;
	}
}
int comparison_function_ascending_uint32 (const void * p_0, const void * p_1)
{
	uint32_t const a0 = *(const uint32_t *) p_0;
	uint32_t const a1 = *(const uint32_t *) p_1;
	
	if( a0 < a1 ) {
		return -1;
	} else if( a0 > a1 ) {
		return 1;
	} else {
		return 0;
	}
}
int comparison_function_ascending_char (const void * p_0, const void * p_1)
{
	char const a0 = *(const char *) p_0;
	char const a1 = *(const char *) p_1;
	
	if( a0 < a1 ) {
		return -1;
	} else if( a0 > a1 ) {
		return 1;
	} else {
		return 0;
	}
}
int comparison_function_ascending_tarr (const void * p_0, const void * p_1) {
	struct tarr const * a0 = ( struct tarr *) p_0;
	struct tarr const * a1 = ( struct tarr *) p_1;
	
	return strcmp( a0->arr_char , a1->arr_char );
}




int comparison_function_descending_float (const void * p_0, const void * p_1) {
	float const a0 = *(const float *) p_0;
	float const a1 = *(const float *) p_1;
	
	if( a0 > a1 ) {
		return -1;
	} else if( a0 < a1 ) {
		return 1;
	} else {
		return 0;
	}
}
int comparison_function_descending_int32 (const void * p_0, const void * p_1) {
	int32_t const a0 = *(const int32_t *) p_0;
	int32_t const a1 = *(const int32_t *) p_1;
	
	if( a0 > a1 ) {
		return -1;
	} else if( a0 < a1 ) {
		return 1;
	} else {
		return 0;
	}
}
int comparison_function_descending_uint32 (const void * p_0, const void * p_1) {
	uint32_t const a0 = *(const uint32_t *) p_0;
	uint32_t const a1 = *(const uint32_t *) p_1;
	
	if( a0 > a1 ) {
		return -1;
	} else if( a0 < a1 ) {
		return 1;
	} else {
		return 0;
	}
}
int comparison_function_descending_char (const void * p_0, const void * p_1) {
	char const a0 = *(const char *) p_0;
	char const a1 = *(const char *) p_1;
	
	if( a0 > a1 ) {
		return -1;
	} else if( a0 < a1 ) {
		return 1;
	} else {
		return 0;
	}
}
int comparison_function_descending_tarr (const void * p_0, const void * p_1) {
	struct tarr * const a0 = ( struct tarr *) p_0;
	struct tarr * const a1 = ( struct tarr *) p_1;
	
	return( -strcmp( a0->arr_char , a1->arr_char ) );
}


static const comparison_function TABLE_OBJTYPE_COMPARISON_FUNCTION_ASCENDING[] = {
	 [type_null]  = 0
	,[type_tarr]  = comparison_function_ascending_tarr
	,[type_float] = comparison_function_ascending_float
	,[type_int32] = comparison_function_ascending_int32
	,[type_uint32]= comparison_function_ascending_uint32
	,[type_char]  = comparison_function_ascending_char
	,[type_int64] = 0
	,[type_uint64] = 0
	,[type_double] = 0
};
static const comparison_function TABLE_OBJTYPE_COMPARISON_FUNCTION_DESCENDING[] = {
	 [type_null]  = 0
	,[type_tarr]  = comparison_function_descending_tarr
	,[type_int32] = comparison_function_descending_int32
	,[type_uint32]= comparison_function_descending_uint32
	,[type_char]  = comparison_function_descending_char
	,[type_int64] = 0
	,[type_uint64] = 0
	,[type_double] = 0
};
static_assert( sizeof(TABLE_OBJTYPE_COMPARISON_FUNCTION_ASCENDING)/sizeof(TABLE_OBJTYPE_COMPARISON_FUNCTION_ASCENDING[0] ) == TYPES_COUNT , "wrong count of comparison functions" );
static_assert( sizeof(TABLE_OBJTYPE_COMPARISON_FUNCTION_DESCENDING)/sizeof(TABLE_OBJTYPE_COMPARISON_FUNCTION_DESCENDING[0] ) == TYPES_COUNT , "wrong count of comparison functions" );


void tarr_sort_in_place( struct tarr * obj )
{
	assert( tarr_is_sortable(obj) );
	assert( TABLE_OBJTYPE_COMPARISON_FUNCTION_ASCENDING[obj->type] != 0 );
	
	
	qsort(
			 obj->ptr
			,obj->size
			,TABLE_OBJTYPE_SIZE[obj->type]
			,TABLE_OBJTYPE_COMPARISON_FUNCTION_ASCENDING[obj->type]
		);
}
void tarr_sort_in_place_descending( struct tarr * obj )
{
	assert( tarr_is_sortable(obj) );
	assert( TABLE_OBJTYPE_COMPARISON_FUNCTION_DESCENDING[obj->type] != 0 );
	
	qsort(
			 obj->ptr
			,obj->size
			,TABLE_OBJTYPE_SIZE[obj->type]
			,TABLE_OBJTYPE_COMPARISON_FUNCTION_DESCENDING[obj->type]
		);
}



struct tarr tarr_sum_of_array( struct tarr * obj )
{
	float as_float;
	int32_t as_int32;
	uint32_t as_uint32;
	
	
	if( !(tarr_has_number_type(obj)) ) {
		printf_warning( "FATAL! tried to get sum of non-number tarr: @%p with type %#x" , obj , obj->type );
		exit( EXIT_FAILURE );
	} else { switch( obj->type ) {
		case type_float:
			as_float = 0.0;
			for( size_t i = 0; i < obj->size; ++i ) {
				as_float += obj->arr_float[i];
			}
			return tarr_create_float( as_float );
		case type_int32:
			as_int32 = 0.0;
			for( size_t i = 0; i < obj->size; ++i ) {
				as_int32 += obj->arr_int32[i];
			}
			return tarr_create_uint32( as_int32 );
		case type_uint32:
			as_uint32 = 0.0;
			for( size_t i = 0; i < obj->size; ++i ) {
				as_uint32 += obj->arr_uint32[i];
			}
			return tarr_create_int32( as_uint32 );
		default:
			printf( "FATAL! unrecognized number-type: @%p type=%#x" , obj , obj->type );
			exit(EXIT_FAILURE);
	} }
}




struct tarr tarr_parse_symbol_as_int32( struct tarr * sym )
{
	assert( sym != 0 );
	assert( sym->type == type_char );
	assert( sym->size > 0 );
	
	int32_t n = strtol( sym->arr_char , 0 , 0 );
	printf( "\n''' %s -> %d '''\n" , sym->arr_char , n );
	
	
	struct tarr obj = tarr_create_int32( n );
	tarr_print( stdout , &obj );
	return obj;
}


struct tarr tarr_autoparse_as_symbol( char * str )
{
	assert( str != 0 );
	
	/* quick guide:
	numbers: starting with a digit or '+' or '-'
		uint64 		base-16 or base-8 (like "0xabcdef123" or "02137" )
		int64  		decimal (like "21" or "-37" or "+44")
		double 		decimal containing '.', like "-21.37" or "9.32"
		// TODO 2018-09-27T23:27:20+02:00: 
		// 1. add scientific and hexadecimal notation for doubles
		// 2. check after parsing, to ensure that no characters are left(that the whole symbol was parsed)
	symbol: everything else by default
	*/
	
	
	struct tarr obj;
	if( isdigit(str[0]) || str[0] == '+' || str[0] == '-' ) {
		char *endptr = 0;
		if( strchr( str , DECIMAL_SEPARATOR ) != 0 ) { // double
			obj = tarr_create_double(strtod( str , &endptr ));
		} else if( str[0] == '0' ) { // uint64
			obj = tarr_create_uint64(strtoul( str , &endptr , 0 ));
		} else {
			obj = tarr_create_int64(strtol( str , &endptr , 0 ));
		}
		
		/* error checking: */
		if( endptr == NULL ) {
			printf_warning( "while converting to %#x: " , obj.type );
			printf_warning( "conversion failed" );
		}
		if( *endptr != '\0' ) {
			printf_warning( "while converting to %#x: " , obj.type );
			printf_warning( "invalid trailing characters after conversion, first invalid is '%c' " , *endptr );
		}
		if( errno == ERANGE ) {
			printf_warning( "error occured while parsing '%s': " , str );
			printf_warning( "ERANGE the resulting value was out of range(overflow or underflow)");
		}
		if( errno == EINVAL ) {
			printf_warning( "error occured while parsing '%s': " , str );
			printf_warning( "EINVAL the given base contains an unsupported value");
		}
	
	} else {
		obj = tarr_create_char_from_cstring(str);
		obj.is_symbol = true;
	}

	if( obj.type == type_null  ) {
		printf_warning( "error occured while parsing '%s': " , str );
		printf_warning( "failed to parse" );
	}
	
	
	return obj;
}


void tarr_autoparse_into_symbol_inplace( struct tarr * tarr )
{
	assert( tarr != 0 );
	assert( tarr->type == type_char );
	assert( tarr->length > 0 );
	
	*tarr = tarr_autoparse_as_symbol( tarr->arr_char );
}


bool tarr_are_same( struct tarr const * const t0 , struct tarr const * const t1 )
{
	return t0 == t1;
}



bool tarr_are_equal( struct tarr * t0 , struct tarr * t1 )
{
	if( t0 == t1 ) {
		return true;
	}
	if( t0 == NULL ) {
		printf_warning( "t0 is null" );
	}
	if( t1 == NULL ) {
		printf_warning( "t1 is null" );
	}
	assert( t0 != NULL );
	assert( t1 != NULL );
	if(  ( t0->type != t1->type )
	  || ( t0->length != t1->length )
	  || ( t0->capacity != t1->capacity )
	  || ( t0->capacity != t1->capacity )
	  || ( t0->is_symbol != t1->is_symbol )
	  ) {
		return false;
	}
	


	if( ( t0->ptr != NULL )
	  &&( t1->ptr != NULL )
	  &&( t0->ptr == t1->ptr ) ) {
		return true;
	}

	assert( t0->type == t1->type );
	switch( t0->type ) {
		case type_tarr:
		case type_float:
		case type_int32:
		case type_uint32:
		case type_char:
			return 0 == 
				memcmp( t0->ptr 
				       ,t1->ptr 
				       ,(t0->length * TABLE_OBJTYPE_SIZE[t0->type] ));
		
		case type_reference_tarr:
			return t0->val_double == t1->val_double;
		
		case type_int64:
			return t0->val_int64 == t1->val_int64;
		
		case type_uint64:
			return t0->val_uint64 == t1->val_uint64;
		
		case type_double:
			return t0->val_double == t1->val_double;
		
		default:
			printf_warning( "unrecognized type %#x" , t0->type );
	}
	
	return false;
}











/*   */
struct tarr * tarr_tarr_get_pointer_to_position( struct tarr obj , size_t const index )
{
	if( obj.type != type_tarr ) {
		return NULL;
	}
	if( obj.ptr == NULL ) {
		return NULL;
	}
	if( index > obj.length ) {
		return NULL;
	}
	
	return &(obj.arr_tarr[index]);
}


size_t tarr_tarr_char_find_position_of_string(
		 struct tarr const obj
		,char * const str )
{
	for( size_t i = 0; i < obj.length; ++i ) {
		if( obj.arr_tarr[i].type == type_char  ) {
			if( strcmp( obj.arr_tarr[i].arr_char , str ) == 0 ) {
				return i;
			}
		}
	}
	return (-1);
}



