#include "primitives.h"





struct tarr primitive_procedure_list( struct tarr * const expr )
{
	assert( expr->type == type_tarr );
	assert( expr->length >= 2 );
	
	struct tarr list = tarr_create_tarr( expr->length - 1 );
	for( size_t i = 1; i < expr->length; ++i ) {
		list.arr_tarr[i-1] = evaluate_0x2( &(expr->arr_tarr[i]) );
	}
	list.length = expr->length - 1;
	
	return list;
}



struct tarr primitive_procedure_quote( struct tarr * const expr )
{
	assert( expr->type == type_tarr );
	assert( expr->length >= 2 );
	
	if( expr->length == 2 ) {
		return tarr_copy( &(expr->arr_tarr[1]) );
	}
	
	struct tarr list = tarr_dumb_copy( expr );
	--(list.length);
	list.arr_tarr = list.arr_tarr + 1;
	
	return tarr_copy( &list );
}




struct tarr primitive_procedure_sum_int64( struct tarr * const expr )
{
	assert( expr->type == type_tarr );
	assert( expr->length >= 2 );
	
	
	int_least64_t result = 0;
	for( size_t i = 1; i < expr->length; ++i ) {
		struct tarr const val = evaluate_0x2( &(expr->arr_tarr[i]) );
		switch( val.type ) {
			case type_int64:
				result += val.val_int64;
				break;
			case type_uint64:
				result += val.val_uint64;
				break;
			default:
				printf_warning( "type mismatch. got %#x, expected %#x or %#x (%s or %s)" , val.type , type_int64 , type_uint64 , type_get_name(type_int64) , type_get_name(type_uint64) );
				exit(EXIT_FAILURE);
		}
	}
	return tarr_create_int64( result );
}


struct tarr primitive_procedure_sum_uint64( struct tarr * const expr )
{
	assert( expr->length >= 2 );
	
	
	uint_least64_t result = 0;
	for( size_t i = 1; i < expr->length; ++i ) {
		struct tarr const val = evaluate_0x2( &(expr->arr_tarr[i]) );
		switch( val.type ) {
			case type_int64:
				result += val.val_int64;
				break;
			case type_uint64:
				result += val.val_uint64;
				break;
			default:
				printf_warning( "type mismatch. got %#x, expected %#x or %#x (%s or %s)" , val.type , type_int64 , type_uint64 , type_get_name(type_int64) , type_get_name(type_uint64) );
				exit(EXIT_FAILURE);
		}
	}
	return tarr_create_uint64( result );
}


struct tarr primitive_procedure_sum_double( struct tarr * const expr )
{
	assert( expr->length >= 2 );
	
	
	double result = 0.0;
	for( size_t i = 1; i < expr->length; ++i ) {
		struct tarr const val = evaluate_0x2( &(expr->arr_tarr[i]) );
		switch( val.type ) {
			case type_double:
				result += val.val_double;
				break;
			case type_int64:
				result += (double)val.val_int64;
				break;
			case type_uint64:
				result += (double)val.val_uint64;
				break;
			default:
				printf_warning( "type mismatch. got %#x, expected %#x or %#x (%s or %s)" , val.type , type_int64 , type_uint64 , type_get_name(type_int64) , type_get_name(type_uint64) );
				exit(EXIT_FAILURE);
		}
	}
	return tarr_create_double( result );
}




struct tarr primitive_procedure_vector_int32( struct tarr * const expr) {
	assert( expr->length > 1 );
	
	
	struct tarr vec_i32 = tarr_create(
			 type_int32
			,(expr->length - 1) );
	for( size_t i = 1; i < expr->length; ++i ) {
		/* don't forget: symbol from expr->arr_tarr[1] gets turned to number at position [0]! */
		struct tarr const val = evaluate_0x2( &(expr->arr_tarr[i]) );
		switch (val.type) {
			case type_int64:
				vec_i32.arr_int32[i-1] = val.val_int64;
				break;
			case type_uint64:
				vec_i32.arr_int32[i-1] = val.val_uint64;
				break;
			default:
				printf_warning( "type mismatch at argument %#zx. got %#x, expected %#x or %#x (type_int64 or type_uin64)" , i , val.type , type_int64 , type_uint64 );
				exit(EXIT_FAILURE);
		}
	}
	
	vec_i32.length = (expr->length - 1);
	return vec_i32;
}


struct tarr primitive_procedure_vector_uint32( struct tarr * const expr) {
	assert( expr->length > 1 );
	
	struct tarr vec_u32 = tarr_create(
			 type_uint32
			,(expr->length - 1) );
	for( size_t i = 1; i < expr->length; ++i ) {
		/* don't forget: symbol from expr->arr_tarr[1] gets turned to number at position [0]! */
		struct tarr const val = evaluate_0x2( &(expr->arr_tarr[i]) );
		switch( val.type ) {
			case type_int64:
				vec_u32.arr_uint32[i-1] = val.val_int64;
				break;
			case type_uint64:
				vec_u32.arr_uint32[i-1] = val.val_uint64;
				break;
			default:
				printf_warning( "type mismatch at argument %#zx. got %#x, expected %#x or %#x (type_int64 or type_uin64)" , i , val.type , type_int64 , type_uint64 );
				exit(EXIT_FAILURE);
		}
	}
	
	vec_u32.length = (expr->length - 1);
	return vec_u32;
}



struct tarr primitive_procedure_vector_float( struct tarr * const expr) {
	assert( expr->length > 1 );
	
	struct tarr vec_f = tarr_create(
			 type_float
			,(expr->length - 1) );
	for( size_t i = 1; i < expr->length; ++i ) {
		/* don't forget: symbol from expr->arr_tarr[1] gets turned to number at position [0]! */
		struct tarr const val = evaluate_0x2( &(expr->arr_tarr[i]) );
		switch( val.type ) {
			case type_double:
				vec_f.arr_float[i-1] = val.val_double;
				break;
			case type_int64:
				vec_f.arr_float[i-1] = val.val_int64;
				break;
			case type_uint64:
				vec_f.arr_float[i-1] = val.val_uint64;
				break;
			default:
				printf_warning( "type mismatch at argument %#zx. got %#x, expected %#x or %#x (type_int64 or type_uin64)" , i , val.type , type_int64 , type_uint64 );
				exit(EXIT_FAILURE);
		}
	}
	
	vec_f.length = (expr->length - 1);
	return vec_f;
}


struct tarr primitive_procedure_int32_enum( struct tarr * const expr) {
	assert( expr != NULL );
	assert( expr->type == type_tarr );
	assert( expr->size == 2 );
	assert( expr->ptr != NULL );
	assert( expr->arr_tarr != NULL );
	assert( expr->arr_tarr[0].type == type_char );
	assert( expr->arr_tarr[0].is_symbol );
	assert( expr->arr_tarr[0].ptr != NULL );
	assert( expr->arr_tarr[0].arr_char != NULL );
	assert( expr->arr_tarr[1].type == type_char );
	assert( expr->arr_tarr[1].is_symbol );
	assert( expr->arr_tarr[1].ptr != NULL );
	assert( expr->arr_tarr[1].arr_char != NULL );
	
	
	struct tarr arg_0 = evaluate_0x2( &(expr->arr_tarr[1]) );
	fprintf( stderr , "in %s : arg_0: " , __func__ );
	tarr_display( stderr , &arg_0 );
	tarr_print( stderr , &arg_0 );
	
	switch( arg_0.type ) {
		case type_int64:
			return tarr_create_int32_enumerate( arg_0.val_int64 );
		case type_uint64:
			return tarr_create_int32_enumerate( arg_0.val_uint64 );
		default:
			printf_warning( "type mismatch: got %#x , expected %#x or %#x type_int64" , arg_0.type , type_int64 , type_uint64 );
			exit(EXIT_FAILURE);
	}
	
}










struct primitive TABLE_PRIMITIVE[] = {
	 { 0 ,  0 , "null" , 0 }
	,{ 1 , -1 , "list" , primitive_procedure_list }
	,{ 1 , -1 , "quote" , primitive_procedure_quote }
	,{ 1 , -1 , "vector:int" , primitive_procedure_vector_int32 }
	,{ 1 , -1 , "vector:uint" , primitive_procedure_vector_uint32 }
	,{ 1 , -1 , "vector:float" , primitive_procedure_vector_float }
	,{ 1 ,  1 , "enum:int" , primitive_procedure_int32_enum }
	,{ 1 , -1 , "sum:int" , primitive_procedure_sum_int64 }
	,{ 1 , -1 , "sum:uint" , primitive_procedure_sum_uint64 }
	,{ 1 , -1 , "sum:double" , primitive_procedure_sum_double }
};
const size_t PRIMITIVES_COUNT = COUNT_OF( TABLE_PRIMITIVE );






const struct primitive * match_primitive_ptr( char * str )
{
	assert( str != NULL );
	// printf_warning( "matching \"%s\"" , str );
	for( size_t i = 0;
	     i < PRIMITIVES_COUNT;
	     ++i ) {
		if( strcmp( str , TABLE_PRIMITIVE[i].name ) == 0 ) {
			return &(TABLE_PRIMITIVE[i]);
		}
	}
	printf_warning( "failed to match \"%s\"\n" , str );
	return NULL;
}



struct tarr evaluate_0x2( struct tarr * expr )
{
	/*
		//	match a primitive to string in 'expr.arr_tarr[0].arr_char'
		if( expr.type = type_tarr ) {
			match_and_execute( expr->arr_tarr[0].arr_char[0] );
		} elif( cur_tarr.type = type_char ) {
			cur_tarr = autoparse_as_symbol( cur_tarr );
		} else {
			cur_tarr = cur_tarr;
		}
	*/
	assert( expr != 0 );
	assert( expr->size > 0 );
	
	if( type_is_number(expr->type) ) {
		return tarr_shallow_copy( expr );
	}
	
	if(  (expr->type == type_tarr)
	  && (expr->arr_tarr[0].type == type_char)
	  && (expr->arr_tarr[0].size > 0)
	  && (expr->arr_tarr[0].arr_char != NULL)
	  && (expr->arr_tarr[0].is_symbol)
			) {
		const struct primitive * primitive = match_primitive_ptr( expr->arr_tarr[0].arr_char );
		assert( primitive != NULL );
		
		
		/* ensure correct number of arguments */
		const int min_args = primitive->min_args;
		const int max_args = primitive->max_args;
		assert( ((int)expr->length - 1) >= min_args );
		if( max_args > -1 ) {
			assert( (((int)expr->length) - 1) <= max_args );
		}
		assert( primitive->proc_ptr != NULL );
		
		return ((primitive->proc_ptr) (expr)) ;
	}
	
	if( expr->type == type_char ) {
		return tarr_autoparse_as_symbol( expr->arr_char );
	}
	
	{
		tarr_display( stderr , expr );
		return tarr_shallow_copy( expr );
	}
}
