#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <stdarg.h>
#include <ctype.h>
#include <errno.h>

#include <assert.h>

#include "tarp.h"



/* TODO:
1. add deep copy and check-equality procedures, which recursively follow through tarrays
2. rename 'struct tarr' to something more sane. maybe 'struct tobj'?
3. overhaul type-names, into something like 'type_vector_float' and 'type_val_int'
4. create tarr_resize() which automatically reallocates object(if( desired_length > capacity) ) and sets it's '.length'
*/


enum TarrType {
	 type_null /* TODO 2018-09-25T1950 deprecate label type_null and change it to type_unitialized */ // type_null shouldn't be used
	,type_tarr
	,type_float
	,type_int32
	,type_uint32
	,type_char
	,type_reference_tarr
	,type_int64
	,type_uint64
	,type_double
	,TYPES_COUNT
};


/* TODO 2018-09-25T2009 add tarr_resize which automatically grows(via tarr_reallocate) the tarr if needed  */

typedef struct tarr Tarr;

struct tarr {
	enum TarrType type;
	
	union {
		size_t length;
		size_t size;
	};
	
	union {
		size_t capacity;
		size_t capa;
	};
	
	bool is_symbol;
	
	void * ptr;
	union {
		void * void_ptr;
		float * arr_float;
		int32_t * arr_int32;
		uint32_t * arr_uint32;
		char * arr_char;
		struct tarr * arr_tarr;
	};
	
	
	union {
		int_least64_t val_int64;
		uint_least64_t val_uint64;
		double val_double;
		struct tarr * reference_tarr;
	};
};

static const struct tarr TARR_NULL = { .type = type_null , .length = 0 , .capacity = 0 , .is_symbol = false , .ptr = 0 , .void_ptr = 0 , .val_uint64 = 0 };





bool type_is_array( enum TarrType const type );
bool type_is_number( enum TarrType const type );
const char * type_get_name( enum TarrType const type );


void types_print_all( FILE * out );
void tarr_display( FILE * out , struct tarr * obj );
void tarr_print( FILE * out , struct tarr * obj );
void tarr_pretty_print( FILE * out , struct tarr * obj , size_t const indent );


void tarr_allocate( struct tarr * tarr , size_t const size );
void tarr_autoset_pointer( struct tarr * tarr );
struct tarr tarr_create_empty( enum TarrType const type );
struct tarr  tarr_create(
		enum TarrType const type
		,size_t const capacity);

void tarr_reallocate( struct tarr * tarr , size_t const size );
void tarr_reallocate_autogrow(
/* doubles tarr->capacity */
		struct tarr * tarr );
void tarr_autogrow_if_needed(
/* if tarr->capacity isn't greater than size, then autogrows it */
		struct tarr * tarr );




void tarr_deallocate( struct tarr * obj ); /* recursively frees dynamic array(s): follows through each array of tarrs to deallocate everything. */
void tarr_deallocate_and_set(struct tarr * obj); /* calls tarr_free_recursively, then changes pointers and capacity to signify null-ness*/
void tarr_delete( struct tarr ** obj ); /* calls tarr_free_recursively then free(*obj) and sets *obj to null   */


struct tarr * tarr_new(
		enum TarrType const type 
		,size_t const capacity );


struct tarr tarr_create_tarr( size_t const capacity );
struct tarr * tarr_new_tarr( size_t const capacity );



struct tarr tarr_create_reference_tarr( struct tarr * ptr_tarr );
struct tarr tarr_create_int64( int_least64_t const n );
struct tarr tarr_create_uint64( uint_least64_t const n );
struct tarr tarr_create_double( double const n );


struct tarr tarr_create_float( float const f );
struct tarr tarr_create_float_vector( size_t const len , ... );

struct tarr tarr_create_int32( int32_t const n );
struct tarr tarr_create_int32_vector( size_t const len , ... );
struct tarr tarr_create_int32_enumerate( size_t const len );

struct tarr tarr_create_uint32( uint32_t const n );
struct tarr tarr_create_uint32_vector( size_t const len , ... );




struct tarr * tarr_string_new_from_cstring(
		char const * cstring );
struct tarr  tarr_create_char_from_cstring( const char * const cstr );
struct tarr tarr_create_char_wrap_cstring( char * cstr );
struct tarr * tarr_new_string_from_cstring( char * const cstr );



struct tarr tarr_dumb_copy( const struct tarr * tarr );
struct tarr tarr_shallow_copy(  const struct tarr * tarr );
struct tarr tarr_copy( const struct tarr * tarr );


void tarr_char_stack_push( struct tarr * tarr , char const ch ) ;

void tarr_tarr_stack_push(
		 struct tarr * tarr
		,struct tarr copied );
void tarr_tarr_stack_emplace(
		 struct tarr * tarr
		,struct tarr * copied );


void tarr_sort_in_place( struct tarr * obj );
void tarr_sort_in_place_descending( struct tarr * obj );



struct tarr tarr_sum_of_array( struct tarr * obj );


struct tarr tarr_parse_symbol_as_int32( struct tarr * sym );

struct tarr tarr_autoparse_as_symbol( char * str );
void tarr_autoparse_into_symbol_inplace( struct tarr * tarr );



bool tarr_are_same( struct tarr const * const t0 , struct tarr const * const t1 );
bool tarr_are_equal(
/* note: checks only shallowly. i.e.: tarray won't be recursively checked */
/* TODO : add proper equaliti-checking of tarrays */
		struct tarr * t0 , struct tarr * t1 );



struct tarr * tarr_tarr_get_pointer_to_position(
		 struct tarr obj
		,size_t const index );

size_t tarr_tarr_char_find_position_of_string(
/* return (-1) upon failure to find a match */
		 struct tarr const obj
		,char * const str );



void tarr_tarr_stack_push_dumb_copy( 
		 struct tarr * tarr 
		,struct tarr * copied
		) __attribute__ ((deprecated ("use tarr_tarr_stack_push or tarr_tarr_stack_emplace") ));
void tarr_tarr_stack_push_shallow_copy(
/* copies only the 'struct tarr' and it's immediate array - doesn't follow through when copying type_tarr */
		 struct tarr * tarr 
		,struct tarr * copied
		) __attribute__ ((deprecated ("use tarr_tarr_stack_push or tarr_tarr_stack_emplace") )) ;
