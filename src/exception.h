#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <setjmp.h>

#include "tarray.h"


struct environment {
	jmp_buf exception_environment;
	// TODO: add some kind of logging // FILE * logfile;
};





void __teh_throw_exception_with_object(
		 struct environment * env
		,struct tarr * obj
		,char * const format 
		, ... )
  __attribute__ ((noreturn, format(printf, 3, 4)) );
#define throw_exception_with_object( env , obj , format , ... ) throw_exception_with_object(env, obj , "in" __func__ ": " format , ##__VA_ARGS__)


#define throw_exception( env , format , ... ) throw_exception_with_object( env , 0 ,  "in" __func__ ": " format , ##__VA_ARGS__  )
